class Creditor < ApplicationRecord
  has_many :payments, dependent: :nullify
	[:sum, :months, :rate, :period, :delay_rate].each do |field|
    validates_numericality_of field, on: :create, greater_than: 0
  end
  attr_accessor :delay_from, :total_paid, :must_be_payed, :last_payment
  DEFAULT_RATE = self.column_defaults['rate']
  DEFAULT_DELAY_RATE = self.column_defaults['delay_rate']

  # выручка от вложения
  def self.receipts(sum, rate)
    (rate * 0.01 * sum.to_i).round(2)
  end

  # средняя прибыльность
  def self.rate_of_return
    list = self.all
    list.reduce(0) do |total, element|
      total + element.rate_of_return
    end / list.size
  end

  # прибыльность
  def rate_of_return
    self.last_payment = self.payments.last
    return (self.created_at + self.period.month < Time.now) ? DEFAULT_DELAY_RATE : DEFAULT_RATE unless self.last_payment
    self.determine_delinquencies!
    if self.delay_from
      ontime_months = self.delay_from - 1
      other_months = self.months - ontime_months
      ((ontime_months * DEFAULT_RATE + other_months * DEFAULT_DELAY_RATE) / self.months).round(2)
    else
      DEFAULT_RATE
    end
  end

  # определить наличие посрочек
  def determine_delinquencies!
    n = 0
    loop do
      n += self.period
      nth_payment_time = self.created_at + n.month
      break if nth_payment_time > Time.now
      self.total_paid = self.fact_payment_until(nth_payment_time)
      break if self.total_paid - self.sum > n * self.periodical_commission
      self.must_be_payed += (self.periodical_payment + self.periodical_commission)
      self.check_delay!(n)
    end
  end

  # проверить просрочен ли хоть один платёж
  def check_delay!(n)
    return if self.delay_from
    return if self.total_paid >= self.must_be_payed
    self.delay_from = n
  end

  # оплачено по задолжности
  def total_paid
    @total_paid || 0
  end

  # должно быть оплачено
  def must_be_payed
    @must_be_payed || 0
  end

  # актуальная процентная ставка
  def actual_rate
    (self.delay_from ? DEFAULT_DELAY_RATE : DEFAULT_RATE) / 100
  end

  # сколько фактически оплачено до переданной даты
  def fact_payment_until(payment_time = Time.now)
    self.payments.where("created_at <= '#{payment_time}'").reduce(0) do |total, payment|
      total + payment.amount
    end
  end

  # периодическая выплата
  def periodical_payment
    self.period.to_f / self.months * self.sum
  end

  # периодическая комиссия
  def periodical_commission
    self.sum * self.actual_rate * self.period / 12.0
  end
end
