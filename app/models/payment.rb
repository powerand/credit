class Payment < ApplicationRecord
  belongs_to :creditor
  validates_numericality_of :amount, on: :create, greater_than: 0
  CURRENCY = '₽'
end
