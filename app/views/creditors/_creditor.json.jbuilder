json.extract! creditor, :id, :name, :sum, :months, :rate, :period, :delay_rate, :created_at, :updated_at
json.url creditor_url(creditor, format: :json)
