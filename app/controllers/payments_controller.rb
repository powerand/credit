class PaymentsController < ApplicationController
  before_action :set_creditor

  def create
    @creditor.payments.create! payments_params
    redirect_to @creditor
  end

  private
    def set_creditor
      @creditor = Creditor.find(params[:creditor_id])
    end

    def payments_params
      params.required(:payment).permit(:amount, :created_at)
    end
end
