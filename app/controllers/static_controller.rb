class StaticController < ApplicationController
  def home
    if (sum = params['sum']) && sum!=''
      @expected_receipts = Creditor.receipts(sum.to_i, Creditor::DEFAULT_RATE)
      @historical_receipts = Creditor.receipts(sum.to_i, Creditor::rate_of_return)
    end
  end
end
