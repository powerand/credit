README
====================

Deployment instructions:


* gpg2 --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
* curl -L https://get.rvm.io | bash -s stable --ruby
* exec bash --login
* rvm install 2.4
* gem install bundler
* yum install -y git
* mkdir -p ~/.ssh
* echo "Host bitbucket.org" >> ~/.ssh/config
* echo "    StrictHostKeyChecking no" >> ~/.ssh/config
* git clone https://bitbucket.org/powerand/credit.git
* cd credit
* yum -y install postgresql postgresql-devel postgresql-server
* service postgresql initdb
* sed -i.bak 's/local   all             all                                     peer/local   all             all                                     trust/g' /var/lib/pgsql/data/pg_hba.conf
* sed -i.bak 's/host    all             all             127.0.0.1\/32            ident/host    all             all             127.0.0.1\/32            trust/g' /var/lib/pgsql/data/pg_hba.conf
* sed -i.bak 's/host    all             all             ::1\/128                 ident/host    all             all             ::1\/128                 trust/g' /var/lib/pgsql/data/pg_hba.conf
* service postgresql restart
* systemctl enable postgresql
* bundle
* rails db:create
* rails db:migrate
* bin/spring stop
* rails server

- - -

Tested OS-es
================================

* CentOS Linux
