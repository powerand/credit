class CreateCreditors < ActiveRecord::Migration[5.1]
  def change
    create_table :creditors do |t|
      t.string :name
      t.integer :sum
      t.integer :months
      t.float :rate, default: 30
      t.integer :period
      t.float :delay_rate, default: 50

      t.timestamps
    end
  end
end
