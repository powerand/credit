# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Payment.delete_all
Creditor.delete_all
AMOUNTS = {
  1 => [191667, 191667, 191667, 191667, 191667, 191667],
  2 => [191667, 191667, 191667, 525000],
  3 => [191667, 191667, 208333, 208333, 208333, 208333],
}
creditors_created_at = Time.now - 6.months
(1..3).each do |i|
  creditor = Creditor.create(name: "UL#{i}", sum: 1000000, months: 6, rate: 30.0, period: 1, delay_rate: 50.0, created_at: creditors_created_at)
  AMOUNTS[i].each_with_index do |amount, n|
    n += 1
    payment_created_at = creditors_created_at + n.months - 10.minutes # do not allow any delay
    payment_created_at += 10.days if i == 3 && n == 3
    creditor.payments.create(amount: amount, created_at: payment_created_at)
  end
end